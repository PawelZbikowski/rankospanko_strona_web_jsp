<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 17.04.2020
  Time: 01:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
          import="edu.ib.hotelIncome.HotelIncomeMethods" %>


<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rankospanko</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Gotu" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }
    </style>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/css/main.css">

    <script src="resources/js/vendor/modernizr-2.8.3.min.js"></script>
  </head>
  <body>

  <%
    String[] avgPFR = request.getParameterValues("avgPriceForRoom");
    String[] counterOR = request.getParameterValues("countOfRooms");
    String[] counterOTR = request.getParameterValues("countOfTakenRooms");
    String[] countOAD = request.getParameterValues("countOfAnalizedDays");
    String[] chooseM = request.getParameterValues("chooseMethod");
    try {
      HotelIncomeMethods hotelIncomeMethods = new HotelIncomeMethods(avgPFR, counterOR,
              counterOTR, countOAD);

      double value = 0;
      String unit = "";
      String countOfDays = "";
      if (chooseM[0].equals("RevPAR")) {
        value = hotelIncomeMethods.revPAR();
        unit = "zł/liczba dni";
        countOfDays = "<h3>Liczba analizowanych dni: " + countOAD[0] + "</h3>";
      }
      else if (chooseM[0].equals("TRevPAR")) {
        value = hotelIncomeMethods.trevPAR();
        unit = "całkowity dochód [zł]/liczba wszystkich pokojów";
      }
      else {
        value = hotelIncomeMethods.revPOR();
        unit = "całkowity dochód [zł]/liczba obsadzonych pokojów";
      }

      pageContext.setAttribute("value", value);
      pageContext.setAttribute("unit", unit);
      pageContext.setAttribute("countOfDays", countOfDays);
    } catch (IllegalArgumentException e) {
      System.out.println("There was a mistake");
    } catch (NullPointerException e) {
      System.out.println("Null bull");
    }
  %>

  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">Rankospanko</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <form class="navbar-form navbar-right" role="form">
          <div class="form-group">
            <input type="text" placeholder="Email" class="form-control">
          </div>
          <div class="form-group">
            <input type="password" placeholder="Hasło" class="form-control">
          </div>
          <button type="submit" class="btn btn-success">Zaloguj się</button>
        </form>
      </div><!--/.navbar-collapse -->
    </div>
  </nav>

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron bg4">
    <div class="container-fluid text-center">
      <h1>Rankospanko</h1>
    </div>
  </div>
  <div class="container-fluid text-center">
    <h1>Wyniki:</h1>
    <h3>Srednia cena za pokój: ${param.avgPriceForRoom} zł</h3>
    <h3>Całkowita liczba pokojów: ${param.countOfRooms}</h3>
    <h3>Liczba obsadzonych pokojów: ${param.countOfTakenRooms}</h3>
    ${countOfDays}
    <h1>Metoda ${param.chooseMethod}:<br>
      ${value} ${unit}</h1>
    <p><a class="btn btn-primary" href="form.html" role="button">Wróć do formularza &raquo;</a></p>
  </div>
  </body>
</html>
