<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.time.LocalDateTime"
%>
<%@ page import="java.time.LocalDate" %>
<%@ page import="static java.time.temporal.ChronoUnit.DAYS" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rankospanko - rezerwacja</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Gotu" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .checked {
            color: orange;
        }
    </style>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/css/main.css">

    <script src="resources/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Rankospanko</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="form">
                <div class="form-group">
                    <input type="text" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Hasło" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Zaloguj się</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron bg5">
    <div class="container-fluid text-center">
        <h1>Rankospanko</h1>
    </div>
</div>

<div class="container text-center bg2">
    <h1>Rezerwacja:</h1>
    <form action="ReserveNightClientServlet" method="get">
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="hotelNameN">Nazwa hotelu:</label>
                <input type="text" class="form-control is-valid" id="hotelNameN" name="hotelNameN" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="firstNameN">Imię:</label>
                <input type="text" class="form-control is-valid" id="firstNameN" name="firstNameN" required>
            </div>
            <div class="col-md-4 mb-3">
                <label for="surnameN">Nazwisko:</label>
                <input type="text" class="form-control is-valid" id="surnameN" name="surnameN" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5 mb-3">
                <label for="emailN"><span class="glyphicon glyphicon-envelope"></span> Adres mailowy:</label>
                <input type="text" class="form-control is-valid" id="emailN" name="emailN" required>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="trip-startN"><span class="glyphicon glyphicon-calendar"></span> Termin rezerwacji od:</label>
                <input type="date" class="form-control is-valid" id="trip-startN" name="trip-startN" required value="<%=LocalDate.now()%>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="trip-endN"><span class="glyphicon glyphicon-calendar"></span> Termin rezerwacji do:</label>
                <input type="date" class="form-control is-valid" id="trip-endN" name="trip-endN" required value="<%=LocalDate.now().plus(7, DAYS)%>">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="chooseRoomN">Wybór rodzaju pokoju:</label>
                <select id="chooseRoomN" name="chooseRoomN" class="form-control">
                    <option value="room-for-two" selected>Pokój dwuosobowy</option>
                    <option value="room-for-three">Pokój trzyosobowy</option>
                    <option value="family-room">Pokój rodzinny (czteroosobowy)</option>
                    <option value="apartment">Apartament (max. sześcioosobowy)</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-lg btn-primary">Rezerwuj</button>
            </div>
        </div>
        <div class="row form-group"></div>
        <div class="row form-group"></div>
        <div class="row form-group">
            <div class="col-md-12">
                <a class="btn btn-lg btn-danger" href="admin-login.html" role="button">Panel Administratora</a>
            </div>
        </div>

    </form>
</div>

</body>
</html>