<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 25.04.2020
  Time: 19:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rankospanko - rezerwacja</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Gotu" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .checked {
            color: orange;
        }
    </style>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/css/main.css">

    <script src="resources/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">Rankospanko</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right" role="form">
                <div class="form-group">
                    <input type="text" placeholder="Email" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Hasło" class="form-control">
                </div>
                <button type="submit" class="btn btn-success">Zaloguj się</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="jumbotron bg5">
    <div class="container-fluid text-center">
        <h1>Rankospanko</h1>
    </div>
</div>

<div class="container text-center bg2">
    <h1>Rezerwacja</h1>
    <h2>Rezerwujący:</h2>
    Imię: ${client.getFirstName()}<br>
    Nazwisko: ${client.getSurname()}<br>
    Adres mailowy: ${client.getEmail()}<br>
    <br>
    <h2>Pokój:</h2>
    Hotel: ${reservation.getHotelName()}<br>
    Rodzaj pokoju: ${typeOfRoom.getTypeOfRoom()}<br>
    Cena za noc: ${typeOfRoom.getPrice()} zł <br>
    <br>
    <h2>Termin wyjazdu:</h2>
    Początek wyjazdu: ${reservation.getTripStart()}<br>
    Koniec wyjazdu: ${reservation.getTripStop()}<br>
    Długość wyjazdu: ${tripDuration} dzień/dni<br>
    Dni do wyjazdu: ${daysLeft} <br>
    <h2>Cena za cały wyjazd: ${wholePrice} zł</h2>
    <p><a class="btn btn-primary" href="reservation.html" role="button">Wróć do formularza rezerwacji &raquo;</a></p>
</div>




</body>
</html>
