<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 02.05.2020
  Time: 23:47
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*, edu.ib.reserveNight.ReservNight" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rankospanko - panel admina</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Gotu" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .checked {
            color: orange;
        }
    </style>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/css/main.css">

    <script src="resources/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Rankospanko</a>
        </div>
    </div>
</nav>

<div class="jumbotron bg6">
    <div class="container-fluid text-center">
        <h1>Rankospanko</h1>
    </div>
</div>

<div class="row form-group"></div>
<div class="row form-group"></div>

<div class="container text-center bg2">
    <h1>Rezerwacje</h1>

    <div class="row form-group"></div>
    <div class="row form-group"></div>
    <div class="row form-group"></div>

    <table class="table table-striped">

        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Imię</th>
            <th scope="col">Nazwisko</th>
            <th scope="col">E-mail</th>
            <th scope="col">Dzień wyjazdu</th>
            <th scope="col">Dzień powrotu</th>
            <th scope="col">Nazwa hotelu</th>
            <th scope="col">Rodzaj pokoju</th>
            <th scope="col">Koszt całkowity</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="tmpReserv" items="${RESERVATIONS_LIST}">

            <%-- definiowanie linkow--%>
            <c:url var="updateLink" value="ReserveNightAdminServlet">
                <c:param name="command" value="LOAD"></c:param>
                <c:param name="id" value="${tmpReserv.idN}"></c:param>
            </c:url>

            <c:url var="deleteLink" value="ReserveNightAdminServlet">
                <c:param name="command" value="DELETE"></c:param>
                <c:param name="id" value="${tmpReserv.idN}"></c:param>
            </c:url>

            <tr>
                <th scope="row">${tmpReserv.idN}</th>
                <td>${tmpReserv.firstNameN}</td>
                <td>${tmpReserv.surnameN}</td>
                <td>${tmpReserv.emailN}</td>
                <td>${tmpReserv.tripStartN}</td>
                <td>${tmpReserv.tripStopN}</td>
                <td>${tmpReserv.hotelNameN}</td>
                <td>${tmpReserv.idOfRoomN}</td>
                <td>${tmpReserv.totalPriceN}</td>
                <td><a href="${updateLink}">
                    <button type="button" class="btn btn-success">Zmień dane</button>
                </a>
                    <a> </a>
                <a href="${deleteLink}"
                    onclick="if (!(confirm('Czy na pewno chcesz usunąć tę rezerwację?'))) return false">
                    <button type="button" class="btn btn-danger">Usuń rezerwację</button>
                </a> </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="row form-group"></div>
    <div class="row form-group"></div>

    <div class="row">
        <div class="container-fluid text-center">

            <div class="col-sm-9">
                <a href="index.html" class="btn btn-lg btn-primary" role="button" aria-disabled="true">Wróć do strony
                    głównej</a>
            </div>
        </div>
    </div>
</div>



</body>
</html>
