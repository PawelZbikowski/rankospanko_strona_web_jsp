<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 03.05.2020
  Time: 00:32
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*, edu.ib.reserveNight.ReservNight" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rankospanko - aktualizacja rezerwacji</title>
    <link rel="shortcut icon" type="image/x-icon" href="resources/img/favicon.ico"/>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Gotu" rel="stylesheet">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <style>
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .checked {
            color: orange;
        }
    </style>
    <link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="resources/css/main.css">

    <script src="resources/js/vendor/modernizr-2.8.3.min.js"></script>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Rankospanko</a>
        </div>
    </div>
</nav>

<div class="jumbotron bg6">
    <div class="container-fluid text-center">
        <h1>Rankospanko</h1>
    </div>
</div>

<div class="row form-group"></div>
<div class="row form-group"></div>

<div class="container text-center bg2">
    <h1>Aktualizuj dane rezerwacji:</h1>
    <form action="ReserveNightAdminServlet" method="get">
        <input type="hidden" name="command" value="UPDATE"/>
        <input type="hidden" name="id" value="${RESERVATION.idN}"/>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="hotelNameN">Nazwa hotelu:</label>
                <input type="text" class="form-control is-valid" id="hotelNameN" name="hotelNameN" required value="${RESERVATION.hotelNameN}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="firstNameN">Imię:</label>
                <input type="text" class="form-control is-valid" id="firstNameN" name="firstNameN" required value="${RESERVATION.firstNameN}">
            </div>
            <div class="col-md-4 mb-3">
                <label for="surnameN">Nazwisko:</label>
                <input type="text" class="form-control is-valid" id="surnameN" name="surnameN" required value="${RESERVATION.surnameN}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5 mb-3">
                <label for="emailN"><span class="glyphicon glyphicon-envelope"></span> Adres mailowy:</label>
                <input type="text" class="form-control is-valid" id="emailN" name="emailN" required value="${RESERVATION.emailN}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="trip-startN"><span class="glyphicon glyphicon-calendar"></span> Termin rezerwacji od:</label>
                <input type="date" class="form-control is-valid" id="trip-startN" name="trip-startN" required value="${RESERVATION.tripStartN}">
            </div>
            <div class="col-md-4 mb-3">
                <label for="trip-endN"><span class="glyphicon glyphicon-calendar"></span> Termin rezerwacji do:</label>
                <input type="date" class="form-control is-valid" id="trip-endN" name="trip-endN" required value="${RESERVATION.tripStopN}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4 mb-3">
                <label for="chooseRoomN">Wybór rodzaju pokoju:</label>
                <select id="chooseRoomN" name="chooseRoomN" class="form-control">
                    <option value="room-for-two" selected>Pokój dwuosobowy</option>
                    <option value="room-for-three">Pokój trzyosobowy</option>
                    <option value="family-room">Pokój rodzinny (czteroosobowy)</option>
                    <option value="apartment">Apartament (max. sześcioosobowy)</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <button type="submit" class="btn btn-success">Aktualizuj dane</button>
            </div>
        </div>


    </form>
</div>

<div class="row form-group"></div>
<div class="row form-group"></div>

<div class="row form-group">
    <div class="col-md-6">
        <a class="btn btn-lg btn-primary" href="ReserveNightAdminServlet" role="button" aria-disabled="true">Wróć do zestawienia</a>
    </div>
</div>

</body>
</html>
