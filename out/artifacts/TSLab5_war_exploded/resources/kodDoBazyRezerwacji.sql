CREATE DATABASE reservations;

USE reservations;

CREATE TABLE typeOfRoom (
idOfRoom int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
typeOf varchar(40) NOT NULL,
priceOfRoom decimal(5,2) NOT NULL
);

INSERT INTO typeOfRoom(typeOf, priceOfRoom) VALUES
("Pokój dwuosobowy", 120.0),
("Pokój trzyosobowy", 180.0),
("Pokój rodzinny (czteroosobowy)", 220.0),
("Apartament (max. sześcioosobowy)", 500.0);

SELECT * FROM typeOfRoom;

CREATE TABLE reserv (
idN int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
firstNameN varchar(30) NOT NULL,
surnameN varchar(45) NOT NULL,
emailN varchar(70) NOT NULL,
tripStartN date NOT NULL,
tripStopN date NOT NULL,
hotelNameN varchar(40) NOT NULL,
idOfRoomN int,
FOREIGN KEY (idOfRoomN)
    REFERENCES typeOfRoom(idOfRoom),
totalPriceN decimal(6,2) NOT NULL
);

drop table reserv;

INSERT INTO reserv(firstNameN, surnameN, emailN, tripStartN, tripStopN, hotelNameN, idOfRoomN, totalPriceN) VALUES
("Paweł", "Zabobon", "pawelzabobon@pwr.edu.pl", "2020-09-08", "2020-09-15", "Malinowy dwór", 1, 960.0),
("Karolina", "Nihilnowi", "knihil24@gmail.com", "2020-07-14", "2020-07-28", "Amejzin Hotel&SPA", 3, 3300.0);
-- ("Zbigniew", "Zbyszkowski", "zbizby12@wp.pl", "2020-08-02", "2020-08-09", "Pensjonat u Jana", 2, 1440.0);

SELECT * FROM reserv;



