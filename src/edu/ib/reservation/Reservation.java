package edu.ib.reservation;

import java.time.LocalDate;

public class Reservation {

    private LocalDate tripStart;
    private LocalDate tripStop;
    private TypeOfRoom typeOfRoom;
    private String hotelName;

    public Reservation(LocalDate tripStart, LocalDate tripStop, TypeOfRoom typeOfRoom, String hotelName) {
        this.tripStart = tripStart;
        this.tripStop = tripStop;
        this.typeOfRoom = typeOfRoom;
        this.hotelName = hotelName;
    }

    public LocalDate getTripStart() {
        return tripStart;
    }

    public void setTripStart(LocalDate tripStart) {
        this.tripStart = tripStart;
    }

    public LocalDate getTripStop() {
        return tripStop;
    }

    public void setTripStop(LocalDate tripStop) {
        this.tripStop = tripStop;
    }

    public TypeOfRoom getTypeOfRoom() {
        return typeOfRoom;
    }

    public void setTypeOfRoom(TypeOfRoom typeOfRoom) {
        this.typeOfRoom = typeOfRoom;
    }

    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "tripStart=" + tripStart +
                ", tripStop=" + tripStop +
                ", typeOfRoom=" + typeOfRoom +
                ", hotelName='" + hotelName + '\'' +
                '}';
    }
}
