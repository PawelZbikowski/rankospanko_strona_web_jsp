package edu.ib.reservation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@WebServlet("/ReservationServlet")
public class ReservationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // krok 1
        Client client = new Client(request.getParameter("firstName"), request.getParameter("surname"), request.getParameter("email"));
        request.setAttribute("client", client);

        String typeOfRoomo = request.getParameter("chooseRoom");

        // parametry servletu
        ServletContext context = getServletContext();
        String priceForRoom = context.getInitParameter(typeOfRoomo);

        // konstruktor klasy TypeOfRoom
        TypeOfRoom typeOfRoom = new TypeOfRoom(typeOfRoomo, Integer.parseInt(priceForRoom));
        request.setAttribute("typeOfRoom", typeOfRoom);

        try {
            LocalDate now = LocalDate.now();
            LocalDate tripStart = LocalDate.parse(request.getParameter("trip-start"));
            LocalDate tripEnd = LocalDate.parse(request.getParameter("trip-end"));

            long tripDuration = ChronoUnit.DAYS.between(tripStart, tripEnd) + 1;
            long daysLeft = ChronoUnit.DAYS.between(now, tripStart);

            int wholePrice = (int) tripDuration * typeOfRoom.getPrice();
            request.setAttribute("wholePrice", wholePrice);

            if (tripDuration < 0) {
                request.setAttribute("tripDuration", "Taki wyjazd nie może się odbyć");
                request.setAttribute("wholePrice", 0);
                request.setAttribute("daysLeft", "Taki wyjazd nie może się odbyć");
            } else if (daysLeft < 0) {
                request.setAttribute("daysLeft", "Wyjazd już się zaczął");
                request.setAttribute("tripDuration", tripDuration);
            } else {
                request.setAttribute("tripDuration", tripDuration);
                request.setAttribute("daysLeft", daysLeft);
            }
            String hotelName = request.getParameter("hotelName");

            // konstruktor klasy Reservation
            Reservation reservation = new Reservation(tripStart, tripEnd, typeOfRoom, hotelName);
            request.setAttribute("reservation", reservation);

        } catch (DateTimeException e) {
            System.out.println("Problem occurred while calculating a date");
        } catch (NullPointerException e) {
            System.out.println("Null value has been spotted");
        }

        // krok 2
        RequestDispatcher dispatcher = request.getRequestDispatcher("/view_reservation.jsp");

        // krok 3
        dispatcher.forward(request, response);
    }
}
