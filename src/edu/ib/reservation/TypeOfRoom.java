package edu.ib.reservation;

public class TypeOfRoom {

    private String typeOfRoom;
    private int price;

    public TypeOfRoom(String typeOfRoom, int price) {
        this.typeOfRoom = typeOfRoom;
        this.price = price;
    }

    public String getTypeOfRoom() {
        if (typeOfRoom.equals("room-for-two")) {
            return "Pokój dwuosobowy";
        } else if (typeOfRoom.equals("room-for-three")) {
            return "Pokój trzyosobowy";
        } else if (typeOfRoom.equals("family-room")) {
            return "Pokój rodzinny (czteroosobowy)";
        } else {
            return "Apartament (max. sześcioosobowy)";
        }
    }

    public void setTypeOfRoom(String typeOfRoom) {
        this.typeOfRoom = typeOfRoom;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "TypeOfRoom{" +
                "typeOfRoom='" + typeOfRoom + '\'' +
                ", price=" + price +
                '}';
    }
}
