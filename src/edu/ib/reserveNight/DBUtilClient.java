package edu.ib.reserveNight;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class DBUtilClient extends DBUtil {

    private DataSource dataSource;

    public DBUtilClient(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void addReservation(ReservNight reservNight) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {
            // polaczenie z BD
            conn = dataSource.getConnection();

            // zapytanie INSERT i ustawienie jego parametrow
            String sql = "INSERT INTO reserv(firstNameN, surnameN, emailN, tripStartN," +
                    " tripStopN, hotelNameN, idOfRoomN, totalPriceN) VALUES (?,?,?,?,?,?,?,?)";

            statement = conn.prepareStatement(sql);
            statement.setString(1, reservNight.getFirstNameN());
            statement.setString(2, reservNight.getSurnameN());
            statement.setString(3, reservNight.getEmailN());
            statement.setString(4, reservNight.getTripStartN());
            statement.setString(5, reservNight.getTripStopN());
            statement.setString(6, reservNight.getHotelNameN());
            statement.setInt(7, reservNight.getIdOfRoomN());
            statement.setDouble(8, reservNight.getTotalPriceN());

            // wykonanie zapytania
            statement.execute();

        } finally {

            close(conn, statement, null);

        }
    }
}
