package edu.ib.reserveNight;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@WebServlet("/ReserveNightAdminServlet")
public class ReserveNightAdminServlet extends HttpServlet {

    private DBUtilAdmin dbUtilAdmin;
    private final String db_url = "jdbc:mysql://localhost:3306/reservations?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";


    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {

            dbUtilAdmin = new DBUtilAdmin(db_url);

        } catch (Exception e) {
            System.out.println("Błąd w metodzie init klasy ReserveNightAdminServlet");
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        String name = request.getParameter("loginInput");
        String password = request.getParameter("passwordInput");

        dbUtilAdmin.setName(name);
        dbUtilAdmin.setPassword(password);

        if (validate(name, password)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");

            List<ReservNight> reservNights = null;

            try {

                reservNights = dbUtilAdmin.getReservations();

            } catch (Exception e) {
                e.printStackTrace();
            }

            // dodanie listy do obiektu
            request.setAttribute("RESERVATIONS_LIST", reservNights);

            dispatcher.forward(request, response);
        }
        else {

            RequestDispatcher dispatcher = request.getRequestDispatcher("/reservation-of-the-night.jsp");
            dispatcher.include(request, response);

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // odczytanie zadania
            String command = request.getParameter("command");

            if (command == null)
                command = "LIST";

            switch (command) {

                case "LIST":
                    listReservations(request, response);
                    break;

                case "LOAD":
                    loadReservations(request, response);
                    break;

                case "UPDATE":
                    updateReservations(request, response);
                    break;

                case "DELETE":
                    deleteReservations(request, response);
                    break;

                default:
                    listReservations(request, response);
            }
        } catch (Exception e) {
            System.out.println("Błąd w metodzie doGet klasy ReserveNightAdminServlet");
            throw new ServletException(e);
        }
    }

    private void loadReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // odczytanie id rezerwacji z formularza
        int id = Integer.parseInt(request.getParameter("id"));

        // pobranie danych rezerwacji z BD
        ReservNight reservNight = dbUtilAdmin.getReservNight(id);

        // przekazanie rezerwacji do obiektu request
        request.setAttribute("RESERVATION", reservNight);

        // wyslanie danych do formularza JSP (update_reservation_form)
        RequestDispatcher dispatcher = request.getRequestDispatcher("/update_reservation_form.jsp");
        dispatcher.forward(request, response);
    }

    private void updateReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // odczytanie danych z formularza
        int id = Integer.parseInt(request.getParameter("id"));
        String hotelNameN = request.getParameter("hotelNameN");
        String firstNameN = request.getParameter("firstNameN");
        String surnameN = request.getParameter("surnameN");
        String emailN = request.getParameter("emailN");
        String tripStartN = request.getParameter("trip-startN");
        String tripStopN = request.getParameter("trip-endN");
        String chooseRoomN = request.getParameter("chooseRoomN");

        // pobranie parametrów servletu (cena za noc za dany pokoj)
        ServletContext context = getServletContext();
        String priceOfRoomN = context.getInitParameter(chooseRoomN);

        Map<Integer, String> typeOfRoomsMap = new HashMap<Integer, String>();

        typeOfRoomsMap.put(1, "room-for-two");
        typeOfRoomsMap.put(2, "room-for-three");
        typeOfRoomsMap.put(3, "family-room");
        typeOfRoomsMap.put(4, "apartment");

        // otrzymanie klucza, któremu odpowiada zadany przez użytkownika typ pokoju
        Stream<Integer> key = getKeysByValue(typeOfRoomsMap, chooseRoomN);
        Integer[] result = key.toArray(Integer[]::new);
        int keyValueForTypeOfRoom = result[0];

        LocalDate tripStart = LocalDate.parse(tripStartN);
        LocalDate tripEnd = LocalDate.parse(tripStopN);

        long tripDuration = ChronoUnit.DAYS.between(tripStart, tripEnd) + 1;

        int totalPrice = (int) tripDuration * Integer.parseInt(priceOfRoomN);

        // utworzenie nowej rezerwacji
        ReservNight reservation = new ReservNight(id, firstNameN, surnameN, emailN,
                tripStartN, tripStopN, hotelNameN, keyValueForTypeOfRoom, totalPrice);

        // uaktualnienie danych w BD
        dbUtilAdmin.updateReservNight(reservation);

        // wyslanie danych do strony z lista rezerwacji
        listReservations(request, response);

    }

    private Stream<Integer> getKeysByValue(Map<Integer, String> map, String value) {
        return map
                .entrySet()
                .stream()
                .filter(entry -> value.equals(entry.getValue()))
                .map(Map.Entry::getKey);
    }

    private void deleteReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // odczytanie danych z formularza
        int id = Integer.parseInt(request.getParameter("id"));

        // usuniecie rezerwacji z BD
        dbUtilAdmin.deleteReservNight(id);

        // wyslanie danych do strony z lista rezerwacji
        listReservations(request, response);
    }

    private void listReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ReservNight> reservNightList = dbUtilAdmin.getReservations();

        // dodanie listy do obiektu zadania
        request.setAttribute("RESERVATIONS_LIST", reservNightList);

        // dodanie request dispatchera
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin_view.jsp");

        // przekazanie do JSP
        requestDispatcher.forward(request, response);
    }

    private boolean validate(String name, String password) {
        boolean status = false;

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            System.out.println("Klasa com.mysql.cj.jdbc.Driver nie została odnaleziona");
            e.printStackTrace();
        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(db_url, name, password);
            status = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }
}
