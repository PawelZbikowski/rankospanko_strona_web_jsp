package edu.ib.reserveNight;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtilAdmin extends DBUtil{

    private String URL;
    private String name;
    private String password;

    public DBUtilAdmin(String URL) {
        this.URL = URL;
    }

    public List<ReservNight> getReservations() throws Exception {

        List<ReservNight> reservNights = new ArrayList<>();

        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // zapytanie SELECT
            String sql = "SELECT * FROM reserv";
            statement = conn.createStatement();

            // wykonanie zapytania SQL
            resultSet = statement.executeQuery(sql);

            // przetworzenie wyniku zapytania
            while (resultSet.next()) {

                // pobranie danych z rzędu
                int idN = resultSet.getInt("idN");
                String firstNameN = resultSet.getString("firstNameN");
                String surnameN = resultSet.getString("surnameN");
                String emailN = resultSet.getString("emailN");
                String tripStartN = resultSet.getString("tripStartN");
                String tripStopN = resultSet.getString("tripStopN");
                String hotelNameN = resultSet.getString("hotelNameN");
                int idOfRoomN = resultSet.getInt("idOfRoomN");
                double totalPriceN = resultSet.getDouble("totalPriceN");

                // dodanie do listy nowego obiektu
                reservNights.add(new ReservNight(idN, firstNameN, surnameN, emailN, tripStartN,
                        tripStopN, hotelNameN, idOfRoomN, totalPriceN));
            }
        } finally {

            // zamkniecie obiektow JDBC
            close(conn, statement, resultSet);
        }

        return reservNights;
    }

    public ReservNight getReservNight(int id) throws Exception {

        ReservNight reservNight = null;

        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // zapytanie SELECT
            String sql = "SELECT * FROM reserv WHERE idN = ?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);

            // wykonanie zapytania
            resultSet = statement.executeQuery();

            // przetworzenie zapytania
            resultSet = statement.executeQuery();

            // przetworzenie wyniku zapytania

            if (resultSet.next()) {

                // pobranie danych z rzędu
                String firstNameN = resultSet.getString("firstNameN");
                String surnameN = resultSet.getString("surnameN");
                String emailN = resultSet.getString("emailN");
                String tripStartN = resultSet.getString("tripStartN");
                String tripStopN = resultSet.getString("tripStopN");
                String hotelNameN = resultSet.getString("hotelNameN");
                int idOfRoomN = resultSet.getInt("idOfRoomN");
                double totalPriceN = resultSet.getDouble("totalPriceN");

                // utworzenie obiektu
                reservNight = new ReservNight(id, firstNameN, surnameN, emailN, tripStartN,
                        tripStopN, hotelNameN, idOfRoomN, totalPriceN);
            }
            else {
                throw new Exception("Could not find phone with id " + id);
            }

            return reservNight;

        } finally {

            // zamkniecie obiektow JDBC
            close(conn, statement, resultSet);
        }
    }

    public void updateReservNight(ReservNight reservNight) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // zapytanie UPDATE
            String sql = "UPDATE reserv SET firstNameN=?, surnameN=?, emailN=?," +
                    " tripStartN=?, tripStopN=?, hotelNameN=?, idOfRoomN=?, " +
                    "totalPriceN = ? WHERE idN=?";

            statement = conn.prepareStatement(sql);
            statement.setString(1, reservNight.getFirstNameN());
            statement.setString(2, reservNight.getSurnameN());
            statement.setString(3, reservNight.getEmailN());
            statement.setString(4, reservNight.getTripStartN());
            statement.setString(5, reservNight.getTripStopN());
            statement.setString(6, reservNight.getHotelNameN());
            statement.setInt(7, reservNight.getIdOfRoomN());
            statement.setDouble(8, reservNight.getTotalPriceN());
            statement.setInt(9, reservNight.getIdN());

            // wykonanie zapytania
            statement.execute();

        } finally {

            // zamkniecie obiektow JDBC
            close(conn, statement, null);
        }
    }

    public void deleteReservNight(int id) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // zapytanie DELETE
            String sql = "DELETE FROM reserv WHERE idN=?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);

            // wykonanie zapytania
            statement.execute();

        } finally {

            // zamknięcie obiektow JDBC
            close(conn, statement, null);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
