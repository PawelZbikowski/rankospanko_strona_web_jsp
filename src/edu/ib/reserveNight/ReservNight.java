package edu.ib.reserveNight;

public class ReservNight {

    private int idN;
    private String firstNameN;
    private String surnameN;
    private String emailN;
    private String tripStartN;
    private String tripStopN;
    private String hotelNameN;
    private int idOfRoomN; // Foreign Key of typeOfRoom table
    private double totalPriceN;

    public ReservNight(int idN, String firstNameN, String surnameN, String emailN, String tripStartN, String tripStopN, String hotelNameN, int idOfRoomN, double totalPriceN) {
        this.idN = idN;
        this.firstNameN = firstNameN;
        this.surnameN = surnameN;
        this.emailN = emailN;
        this.tripStartN = tripStartN;
        this.tripStopN = tripStopN;
        this.hotelNameN = hotelNameN;
        this.idOfRoomN = idOfRoomN;
        this.totalPriceN = totalPriceN;
    }

    public ReservNight(String firstNameN, String surnameN, String emailN, String tripStartN, String tripStopN, String hotelNameN, int idOfRoomN, double totalPriceN) {
        this.firstNameN = firstNameN;
        this.surnameN = surnameN;
        this.emailN = emailN;
        this.tripStartN = tripStartN;
        this.tripStopN = tripStopN;
        this.hotelNameN = hotelNameN;
        this.idOfRoomN = idOfRoomN;
        this.totalPriceN = totalPriceN;
    }

    public int getIdN() {
        return idN;
    }

    public void setIdN(int idN) {
        this.idN = idN;
    }

    public String getFirstNameN() {
        return firstNameN;
    }

    public void setFirstNameN(String firstNameN) {
        this.firstNameN = firstNameN;
    }

    public String getSurnameN() {
        return surnameN;
    }

    public void setSurnameN(String surnameN) {
        this.surnameN = surnameN;
    }

    public String getEmailN() {
        return emailN;
    }

    public void setEmailN(String emailN) {
        this.emailN = emailN;
    }

    public String getTripStartN() {
        return tripStartN;
    }

    public void setTripStartN(String tripStartN) {
        this.tripStartN = tripStartN;
    }

    public String getTripStopN() {
        return tripStopN;
    }

    public void setTripStopN(String tripStopN) {
        this.tripStopN = tripStopN;
    }

    public String getHotelNameN() {
        return hotelNameN;
    }

    public void setHotelNameN(String hotelNameN) {
        this.hotelNameN = hotelNameN;
    }

    public int getIdOfRoomN() {
        return idOfRoomN;
    }

    public void setIdOfRoomN(int idOfRoomN) {
        this.idOfRoomN = idOfRoomN;
    }

    public double getTotalPriceN() {
        return totalPriceN;
    }

    public void setTotalPriceN(double totalPriceN) {
        this.totalPriceN = totalPriceN;
    }

    @Override
    public String toString() {
        return "ReservNight{" +
                "idN=" + idN +
                ", firstNameN='" + firstNameN + '\'' +
                ", surnameN='" + surnameN + '\'' +
                ", emailN='" + emailN + '\'' +
                ", tripStartN='" + tripStartN + '\'' +
                ", tripStopN='" + tripStopN + '\'' +
                ", hotelNameN='" + hotelNameN + '\'' +
                ", idOfRoomN=" + idOfRoomN +
                ", totalPriceN=" + totalPriceN +
                '}';
    }
}
