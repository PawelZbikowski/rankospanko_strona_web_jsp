package edu.ib.reserveNight;

import edu.ib.reservation.Reservation;
import edu.ib.reservation.TypeOfRoom;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@WebServlet("/ReserveNightClientServlet")
public class ReserveNightClientServlet extends HttpServlet {

    private DataSource dataSource;
    private DBUtilClient dbUtil;

    public ReserveNightClientServlet() {

        // Obtain our environment naming context
        Context initCtx = null;
        try {
            initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            // look up our data source
            dataSource = (DataSource) envCtx.lookup("jdbc/reservation_web_app");

        } catch (NamingException e) {
            e.printStackTrace();
            System.out.println("Błąd w konstruktorze ReserveNightClientServlet");
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();

        try {

            dbUtil = new DBUtilClient(dataSource);

        } catch (Exception e) {
            System.out.println("Błąd w metodzie init klasy ReserveNightClientServlet");
            throw new ServletException(e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            // dodanie rezerwacji (MVC)
            addReservations(request, response);
        } catch (Exception e) {
            System.out.println("Błąd w metodzie doGet klasy ReserveNightClientServlet");
            e.printStackTrace();
        }
    }

    private void addReservations(HttpServletRequest request, HttpServletResponse response) throws Exception{

        try {
            // pobranie parametrow z formularza jsp (widoku)
            String firstNameN = request.getParameter("firstNameN");
            String surnameN = request.getParameter("surnameN");
            String emailN = request.getParameter("emailN");
            String tripStartN = request.getParameter("trip-startN");
            String tripStopN = request.getParameter("trip-endN");
            String hotelNameN = request.getParameter("hotelNameN");
            String typeOfRoomN = request.getParameter("chooseRoomN");

            // pobranie parametrów servletu (cena za noc za dany pokoj)
            ServletContext context = getServletContext();
            String priceOfRoomN = context.getInitParameter(typeOfRoomN);

            Map<Integer, String> typeOfRoomsMap = new HashMap<Integer, String>();

            typeOfRoomsMap.put(1, "room-for-two");
            typeOfRoomsMap.put(2, "room-for-three");
            typeOfRoomsMap.put(3, "family-room");
            typeOfRoomsMap.put(4, "apartment");

            // otrzymanie klucza, któremu odpowiada zadany przez użytkownika typ pokoju
            Stream<Integer> key = getKeysByValue(typeOfRoomsMap, typeOfRoomN);
            Integer[] result = key.toArray(Integer[]::new);
            int keyValueForTypeOfRoom = result[0];


            LocalDate now = LocalDate.now();
            LocalDate tripStart = LocalDate.parse(tripStartN);
            LocalDate tripEnd = LocalDate.parse(tripStopN);

            long tripDuration = ChronoUnit.DAYS.between(tripStart, tripEnd) + 1;
            long daysLeft = ChronoUnit.DAYS.between(now, tripStart);

            int totalPrice = (int) tripDuration * Integer.parseInt(priceOfRoomN);

            if (tripDuration < 0) {
                request.setAttribute("message", "Taki wyjazd nie może się odbyć (data wyjazdu po dacie przyjazdu)");
            } else if (daysLeft < 0) {
                request.setAttribute("message", "Proszę wybrać późniejszą datę wyjazdu");
            } else {
                request.setAttribute("message", "Pomyślnie dodano rezerwację! <br>Wyjazd odbędzie się za " + daysLeft + " dni i potrwa "+ tripDuration + " dni.");
                // konstruktor klasy ReservNight
                ReservNight reservation = new ReservNight(firstNameN, surnameN, emailN, tripStartN, tripStopN, hotelNameN, keyValueForTypeOfRoom, totalPrice);
                TypeOfRoom typeOfRoom = new TypeOfRoom(typeOfRoomN, Integer.parseInt(priceOfRoomN));

                request.setAttribute("typeOfRoom", typeOfRoom);
                request.setAttribute("tripDuration", tripDuration);
                request.setAttribute("daysLeft", daysLeft);
                request.setAttribute("totalPrice", totalPrice);
                request.setAttribute("reservation", reservation);

                // dodanie nowego obiektu do BD
                dbUtil.addReservation(reservation);
                }

        } catch (DateTimeException e) {
            System.out.println("Problem occurred while calculating a date");
        } catch (NullPointerException e) {
            System.out.println("Null value has been spotted");
        }

        // krok 2
        RequestDispatcher dispatcher = request.getRequestDispatcher("/client-view.jsp");

        // krok 3
        dispatcher.forward(request, response);

    }

    private <Integer, String> Stream<Integer> getKeysByValue(Map<Integer, String> map, String value) {
        return map
                .entrySet()
                .stream()
                .filter(entry -> value.equals(entry.getValue()))
                .map(Map.Entry::getKey);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
