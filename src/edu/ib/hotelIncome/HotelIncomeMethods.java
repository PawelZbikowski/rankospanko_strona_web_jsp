package edu.ib.hotelIncome;

import java.util.Arrays;

public class HotelIncomeMethods {

    private int [] avgPriceForRoom;
    private int [] countOfRooms;
    private int [] countOfTakenRooms;
    private int [] countOfAnalizedDays;

    public HotelIncomeMethods(String[] avgPriceForRoom, String[] countOfRooms, String[] countOfTakenRooms, String[] countOfAnalizedDays) throws IllegalArgumentException{
        if (avgPriceForRoom[0].equals("") || avgPriceForRoom[0].matches("^[a-zA-Z]+$")){
            this.avgPriceForRoom = new int[]{0};
        } else {
            this.avgPriceForRoom = Arrays.asList(avgPriceForRoom).stream().mapToInt(Integer::parseInt).toArray();
        }
        if (countOfRooms[0].equals("") || countOfRooms[0].matches("^[a-zA-Z]+$")){
            this.countOfRooms = new int[]{0};
        }
        else {
            this.countOfRooms = Arrays.asList(countOfRooms).stream().mapToInt(Integer::parseInt).toArray();
        }
        if (countOfTakenRooms[0].equals("") || countOfTakenRooms[0].matches("^[a-zA-Z]+$")){
            this.countOfTakenRooms = new int[]{0};
        }
        else {
            this.countOfTakenRooms = Arrays.asList(countOfTakenRooms).stream().mapToInt(Integer::parseInt).toArray();
        }
        if (countOfAnalizedDays[0].equals("") || countOfAnalizedDays[0].matches("^[a-zA-Z]+$")){
            this.countOfAnalizedDays = new int[]{0};
        } else {
            this.countOfAnalizedDays = Arrays.asList(countOfAnalizedDays).stream().mapToInt(Integer::parseInt).toArray();
        }

    }

    public HotelIncomeMethods() {
    }

    public int[] getAvgPriceForRoom() {
        return avgPriceForRoom;
    }

    public void setAvgPriceForRoom(int[] avgPriceForRoom) {
        this.avgPriceForRoom = avgPriceForRoom;
    }

    public int[] getCountOfRooms() {
        return countOfRooms;
    }

    public void setCountOfRooms(int[] countOfRooms) {
        this.countOfRooms = countOfRooms;
    }

    public int[] getCountOfTakenRooms() {
        return countOfTakenRooms;
    }

    public void setCountOfTakenRooms(int[] countOfTakenRooms) {
        this.countOfTakenRooms = countOfTakenRooms;
    }

    public int[] getCountOfAnalizedDays() {
        return countOfAnalizedDays;
    }

    public void setCountOfAnalizedDays(int[] countOfAnalizedDays) {
        this.countOfAnalizedDays = countOfAnalizedDays;
    }

    public double revPAR() {
        if (countOfRooms[0] == 0 || countOfTakenRooms[0] > countOfRooms[0]) {
            System.out.println("denominator is equal to zero or count of taken rooms is bigger than count of all rooms");
            return 0;
        } else {
            if (countOfAnalizedDays[0] == 0) {
                return avgPriceForRoom[0] * ((double) countOfTakenRooms[0] / (double) countOfRooms[0]);
            }
            else {
                return countOfAnalizedDays[0] * avgPriceForRoom[0] * ((double) countOfTakenRooms[0] / (double) countOfRooms[0]);
            }
        }
    }

    public double trevPAR() {
        try {
            if (countOfTakenRooms[0] > countOfRooms[0]){
                System.out.println("Count of taken rooms is bigger than count of all rooms");
                return 0;
            }
            else {
                return (0.92 * (double) avgPriceForRoom[0] * (double) countOfTakenRooms[0]) / (double) countOfRooms[0];
            }
        } catch (ArithmeticException e) {
            System.out.println("denominator is equal to zero");
            return 0;
        }

    }

    public double revPOR() {
        try {
            if (countOfTakenRooms[0] > countOfRooms[0]){
                System.out.println("Count of taken rooms is bigger than count of all rooms");
                return 0;
            }
            else {
                return (0.92 * (double) avgPriceForRoom[0] * (double) countOfTakenRooms[0]) / (double) countOfTakenRooms[0];
            }
        } catch (ArithmeticException e) {
            System.out.println("denominator is equal to zero");
            return 0;
        }
    }
}
